// http://eslint.org/docs/user-guide/configuring

module.exports = {
  globals: {
    'createjs': true,
    'game': true,
    '_': true,
    'Phaser': true,
    'loadJSON': true,
    'path': true,
    'axios': true,
    Benchmark: true,
    fs: true,
    Q: true,
    noop: true,
    PIXI: true
  },
  root: true,
  parser: 'babel-eslint',
  parserOptions: {
    sourceType: 'module'
  },
  env: {
    browser: true,
  },
  // https://github.com/feross/standard/blob/master/RULES.md#javascript-standard-style
  extends: [
    'standard'
  ],
  plugins: [
    'html',
    'flow'
  ],
  // add your custom rules here
  'rules': {
    'no-undef': 0,
    // 'unused-function-argument': 0,
    'no-unused-vars': 0,
    'no-useless-rename': 0,
    // allow paren-less arrow functions
    'arrow-parens': 0,
    // allow async-await
    'generator-star-spacing': 0,
    // allow debugger during development
    'no-debugger': process.env.NODE_ENV === 'production' ? 2 : 0,
    'brace-style': 0
  }
}
