// @flow

export type direction = 'n' | 's' | 'e' | 'w'

export type Stat = {|
  hp: number,
  attack: number,
  defense: number,
  specialAttack: number,
  specialDefense: number,
  speed: number
|}

export type concatenatedProperty = {
  value?: string,
  prepend?: Array<string>,
  append?: Array<string>,
  options?: Object
}
