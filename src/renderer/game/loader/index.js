// @flow

import {memoryMiddlewareFactory} from 'resource-loader/src/middlewares/caching/memory'

export class Loader extends PIXI.loaders.Loader {
  constructor (dir: string, ext: string) {
    super(dir)
    this.pre(memoryMiddlewareFactory())
    this.onError.add((...args) => console.log(...args))
    this.dir = dir
    this.ext = ext
  }

  load (id: string): Promise<Object> {
    return new Promise((resolve, reject) => {
      this.add(id, `${id}.${this.ext}`, {crossOrigin: true}, resolve)
      super.load()
    })
  }

  /**
   * Gets a promise for a resource.
   * @param  {string}   id
   * @return {Promise<*>}
   */
  get (id: string): Promise<*> {
    return this.load(id).then(payload => payload.data)
  }

  dir: string
  ext: string
}

export class DataLoader extends Loader {
  constructor (dir: string, ext: string, klass: Function, argBuilder: (Promise<*> => void, Object) => Array<*> = (hlp, json) => [hlp, json]) {
    super(dir, ext)
    this.pre(memoryMiddlewareFactory())
    this.onError.add((...args) => console.log(...args))
    this.argBuilder = argBuilder
    this.Class = klass
  }

  /**
   * Gets a promise for a resource.
   * @param  {string}   id
   * @param  {Function} argBuilder
   * @return {Promise<*>}
   */
  get (id: string, argBuilder?: Function): Promise<*> {
    const buildArgs = argBuilder || this.argBuilder
    return new Promise((resolve, reject) => {
      let promise: Promise<*>
      super.get(id).then(json => {
        const resource = new this.Class(...buildArgs(p => { promise = p }, json))
        promise ? promise.then(() => resolve(resource)).catch(reject) : resolve(resource)
      }).catch(reject)
    })
  }

  Class: Function
  argBuilder: (Promise<*> => void, Object) => Array<*>
}
