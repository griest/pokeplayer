// @flow

import Game from '@/game'
import Area from './area'
import Player from './player'
import Scene from '@/game/scene'

export default class Overworld extends Scene {
  static Area = Area
  static Player = Player

  constructor (handleLoadPromise: Promise<*> => void, {
    area,
    map: currentMap,
    playerX,
    playerY
  }: Object) {
    const loadPromises = []
    super(new PIXI.Container(), {width: area.width, height: area.height})
    this.area = area

    loadPromises.push(game.getResource('map', currentMap).then((map) => {
      this.currentMap = map

      this.playerPos = this.area.absolute(map, playerX, playerY)
      this.player = new Player(this.playerPos)
      this.playerLayer.addChild(this.player.displayObject)

      const x = Math.floor(this.numVisibleTiles(game.view.width) / 2) - this.playerPos.x
      const y = Math.floor(this.numVisibleTiles(game.view.height) / 2) - this.playerPos.y

      this.setPos(x, y)

      this.buildArea()

      this.addChild(this.bottomLayer, this.playerLayer, this.topLayer)
    }))

    handleLoadPromise(Promise.all(loadPromises))
  }

  buildArea (): void {
    this.bottomLayer.removeChildren()
    this.topLayer.removeChildren()

    this.area.data.each((e, i) => {
      if (e) {
        const bottom = _.compact(e.bottomLayer)
        const top = _.compact(e.topLayer)
        if (bottom.length > 0) this.bottomLayer.addChild(...bottom)
        if (top.length > 0) this.topLayer.addChild(...top)
      }
    })
  }

  numVisibleTiles (length: number): number {
    return length / game.constants.tileSize
  }

  resize (width: number, height: number): void {
    super.resize(...arguments)
    const x = Math.floor(this.numVisibleTiles(game.renderer.width) / 2) - this.playerPos.x
    const y = Math.floor(this.numVisibleTiles(game.renderer.height) / 2) - this.playerPos.y
    this.setPos(x, y)
  }

  handleDirectionCommand (command: string): void {
    this.blockInput = true
    this.player.move(command, 'walk').then(() => { this.blockInput = false })
    this.previousDirectionCommand = command
  }

  handleNoDirectionCommand (): void {
    if (!this.player.displayObject.paused) {
      this.player.stopMove()
      this.previousDirectionCommand = null
    }
  }

  playerTweened ({axis, amount, speed}: Object): void {
    this.playerPos[axis] += amount
    this.pan({axis, amount: -amount, speed})
  }

  pan ({axis, amount, speed}: Object): void {
    this.tweenMove({amount, speed, axis})
  }

  handleButtonCommand (command: string): void {
    // noop
  }

  cache (): void {
    this.cacheChildren(this.bottomLayer, this.topLayer)
  }

  cacheChildren (...children: Array<PIXI.DisplayObject>): void {
    _.each(children, layer => {
      layer.cacheAsBitmap = false
      layer.cacheAsBitmap = true
    })
  }

  boundaries: Object
  currentMap: Area.Map
  area: Area
  player: Player
  playerPos: Object
  bottomLayer = new PIXI.Container()
  topLayer = new PIXI.Container()
  playerLayer = new PIXI.Container()
}
