// @flow

export default class Tile {
  constructor ({
    image,
    terrainTag,
    passage
  }: Object) {
    this.image = image
    this.terrainTag = terrainTag
    this.passage = passage
  }

  load (): Promise<PIXI.Texture> {
    if (!this.texturePromise) {
      this.texturePromise = new Promise((resolve, reject) => {
        const img = new Image()
        img.onload = resolve(img)
        img.onerror = reject
        img.crossOrigin = 'Anonymous'
        img.src = game.projectDir + '/data/tileset/' + this.image
      }).then(img => {
        this.texture = new PIXI.Texture(new PIXI.BaseTexture(img))
        return img
      })
    }
    return this.texturePromise
  }

  getSprite (): PIXI.extras.TilingSprite {
    const sprite = new PIXI.extras.TilingSprite(this.texture, game.constants.tileSize, game.constants.tileSize)
    sprite.tile = this
    return sprite
  }

  image: string
  texture: PIXI.Texture
  texturePromise: Promise<PIXI.Texture>
  terrainTag: number
  passage: number
}
