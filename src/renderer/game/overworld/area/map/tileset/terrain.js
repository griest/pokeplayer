// @flow

export default class Terrain {
  name: string
  tile: number
}
