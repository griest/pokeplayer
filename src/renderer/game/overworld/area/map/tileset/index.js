// @flow

import Terrain from '@/game/overworld/area/map/tileset/terrain'
import Tile from '@/game/overworld/area/map/tileset/tile'

export default class Tileset {
  static Terrain = Terrain
  static Tile = Tile

  constructor (hlp: Function, {
    tilewidth,
    tileheight,
    columns,
    margin,
    spacing,
    type,
    grid,
    name,
    tiles,
    tileproperties,
    properties,
    tilecount
  }: Object) {
    this.tileWidth = tilewidth
    this.tileHeight = tileheight
    this.columns = columns
    this.margin = margin
    this.spacing = spacing
    this.type = type
    this.grid = grid
    this.name = name
    this.numAutotiles = properties.numAutotiles
    this.tileCount = tilecount
    this.tiles = _.mapValues(tiles, (tile, key) => new Tile(_.assign({}, tile, tileproperties[key])))
  }

  tileWidth: number
  tileHeight: number
  columns: number
  margin: number
  spacing: number
  type: string
  grid: {|
    height: number,
    orientation: string,
    width: number
  |}
  name: string
  tiles: {[string]: Tile}
  numAutotiles: number
  tileCount: number
  terrains: ?Array<Terrain>

  Tile: Class<Tile> = Tile
}
