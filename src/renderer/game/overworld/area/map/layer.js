// @flow

import Game from '@/game'

export default class Layer {
  constructor (map: Game.Overworld.Area.Map, handleLoadPromise: Promise<*> => mixed, {
    width,
    height,
    name,
    type,
    visible,
    x,
    y,
    properties,
    opacity,
    draworder
  }: Object) {
    this.name = name
    this.type = type
    this.x = x
    this.y = y
    this.height = height
    this.width = width
    this.visible = visible
    this.alpha = opacity
    this.properties = properties
    this.drawOrder = draworder
    this.data = new Game.util.Table()
  }

  data: Game.util.Table<Game.Overworld.Area.Map.Tileset.Tile | Game.Overworld.Area.Map.ObjectLayer.Object>
  width: number
  height: number
  name: string
  type: 'tilelayer' | 'objectgroup' | 'imagelayer'
  visible: boolean = true
  x: number = 0
  y: number = 0
  properties: Object = {}
  alpha: number = 1 // between 0 and 1
  drawOrder: 'topdown' | 'index' = 'topdown'
}
