// @flow

import Layer from '@/game/overworld/area/map/layer'
import Tileset from '@/game/overworld/area/map/tileset'
import TileLayer from '@/game/overworld/area/map/tile-layer'
import ObjectLayer from '@/game/overworld/area/map/object-layer'

export default class Map {
  static Layer = Layer
  static TileLayer = TileLayer
  static ObjectLayer = ObjectLayer
  static Tileset = Tileset

  constructor (handleLoadPromise: Promise<*> => mixed, {
    width,
    height,
    layers,
    tilesets,
    properties
  }: Object) {
    this.id = properties.id
    this.width = width
    this.height = height
    this.properties = properties

    handleLoadPromise(new Promise((resolve, reject) => {
      const loadPromises = []

      Promise.all(_.map(tilesets, tileset => {
        return game.getResource('tileset', game.constructor.ResourceManager.id(tileset.source)).then(ts => {
          this.tilesets.push({firstGID: tileset.firstgid, tileset: ts})
        }).catch(reject)
      })).then(() => {
        this.layers = _.map(layers, layer => {
          if (layer.type === 'tilelayer') {
            return new TileLayer(this, promise => loadPromises.push(promise), layer)
          } else if (layer.type === 'objectgroup') {
            return new ObjectLayer(this, promise => loadPromises.push(promise), layer)
          }
        })
      }).then(() => {
        this.bottomLayers = this.layers.slice(0, 2)
        this.topLayers = this.layers.slice(2, 3)

        Promise.all(loadPromises).then(resolve).catch(reject)
      }).catch(reject)
    }))
  }

  getTopObjectAt (x: number, y: number): ?(Tileset.Tile | ObjectLayer.Object) {
    let tile = null
    let index = this.layers.length - 1
    while (!tile && index >= 0) {
      try { tile = this.layers[index].data.get(x, y) } catch (e) {}
      index -= 1
    }
    return tile
  }

  getObjectsAt (x: number, y: number): Array<?(Tileset.Tile | ObjectLayer.Object)> {
    return [...this.getBottomLayerObjectsAt(x, y), ...this.getTopLayerObjectsAt(x, y)]
  }

  getBottomLayerSpritesAt (x: number, y: number): Array<?PIXI.Sprite> {
    return _.map(this.bottomLayers, layer => layer.getSprite(x, y))
  }

  getTopLayerSpritesAt (x: number, y: number): Array<?PIXI.Sprite> {
    return _.map(this.topLayers, layer => layer.getSprite(x, y))
  }

  getBottomLayerObjectsAt (x: number, y: number): Array<?(Tileset.Tile | ObjectLayer.Object)> {
    return _.map(this.bottomLayers, layer => layer.data.get(x, y))
  }

  getTopLayerObjectsAt (x: number, y: number): Array<?(Tileset.Tile | ObjectLayer.Object)> {
    return _.map(this.topLayers, layer => layer.data.get(x, y))
  }

  id: string
  width: number = 1
  height: number = 1
  tileWidth: number = 32
  tileHeight: number = 32
  orientation: 'orthongonal' | 'isometric' | 'staggered' = 'orthongonal'
  layers: Array<Layer> = []
  bottomLayers: Array<Layer> = []
  topLayers: Array<Layer> = []
  tilesets: Array<Object> = []
  backgroundColor: ?string
  renderOrder: ?string
  nextObjectID: ?number
  properties: Object = {}
  music: Array<string> = []
  events: Array<string> = []
  connections: {[string]: {|
    side: string,
    tile: string,
    otherSide: string,
    otherTile: string
  |}} = {}
}
