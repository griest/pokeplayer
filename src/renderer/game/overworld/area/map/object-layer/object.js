// @flow

import DisplayableModel from '@/game/displayable-model'

export default class MapObject extends DisplayableModel {
  constructor ({
    id,
    width,
    height,
    name,
    visible = true,
    type,
    x = 0,
    y = 0,
    rotation,
    gid,
    properties = {},
    images = [],
    animations = [],
    frames = {},
    framerate = 0.066
  }: Object) {
    super(new PIXI.Actor(images, game.ticker), {
      from: 'visible',
      value: visible
    }, {
      from: 'animationSpeed',
      value: framerate
    })
    this.displayObject.addClips(...animations)
    this.setX(x)
    this.setY(y)
    this.id = id
    this.gid = gid
    this.name = name
    this.type = type
    this.rotation = rotation
    this.properties = properties
  }

  play (animationOrFrame?: string | number): void {
    animationOrFrame ? this.displayObject.gotoAndPlay(animationOrFrame) : this.displayObject.play()
  }

  stop (animationOrFrame?: string | number): void {
    animationOrFrame ? this.displayObject.gotoAndStop(animationOrFrame) : this.displayObject.stop()
  }

  id: number
  name: string
  type: string
  visible: boolean
  rotation: number
  gid: number
  properties: Object
  animationSpeed: number
  frameToggle: boolean = true
}
