// @flow

import Layer from '@/game/overworld/area/map/layer'
import MapObject from '@/game/overworld/area/map/object-layer/object'

export default class ObjectLayer extends Layer {
  static Object = MapObject

  constructor (map: Game.Overworld.Area.Map, handleLoadPromise: Promise<*> => mixed, {
    width,
    height,
    name,
    type,
    visible,
    x,
    y,
    properties,
    opacity,
    draworder,
    objects,
    events
  }: Object) {
    super(...arguments)
    // this.data = objects
  }
}
