// @flow

import MapObject from '@/game/overworld/area/map/object-layer/object'

import type {direction} from '@/game/types'

export default class Actor extends MapObject {
  constructor ({
    id,
    width,
    height,
    name,
    visible,
    type,
    x,
    y,
    rotation,
    gid,
    properties,
    dir = game.paths.actors,
    images,
    animations,
    frames,
    framerate
  }: Object) {
    super(...arguments)
  }

  animateMove ({
    animation,
    shouldPlayAnimation,
    speed,
    direction,
    amount: moveAmount,
    shouldTween,
    afterAnimation,
    beforeTween
  }: Object): Promise<*> {
    const axis = this.directionAxisMap[direction].axis
    const amount = moveAmount || this.directionAxisMap[direction].amount
    const willTween = shouldTween || this.canMove(direction)
    const payload = {...arguments[0], ...{axis, amount, willTween}}

    if (shouldPlayAnimation) {
      this.direction = direction
      this.play(animation)
      if (afterAnimation) afterAnimation(payload)
    }
    return new Promise((resolve, reject) => {
      if (willTween) {
        if (beforeTween) beforeTween(payload)
        this.tweenMove({
          axis,
          amount,
          speed,
          callback: () => resolve(payload)
        })
      } else { resolve(payload) }
    })
  }

  canMove (direction: string = this.direction): boolean {
    const {axis, amount} = this.directionAxisMap[direction]
    const {map, x, y} = game.scene.area.relative(axis === 'x' ? game.scene.playerPos.x + amount : game.scene.playerPos.x, axis === 'y' ? game.scene.playerPos.y + amount : game.scene.playerPos.y)

    try { return map.getTopObjectAt(x, y).passage === 0 } catch (e) { return false }
  }

  setX (x: number): void {
    this.x = x
    this.displayObject.x = x * game.constants.tileSize
  }

  setY (y: number): void {
    this.y = y
    this.displayObject.y = y * game.constants.tileSize - game.constants.tileSize / 2
  }

  direction: direction = 's'
  directionAxisMap = {
    n: {
      axis: 'y',
      amount: -1
    },
    s: {
      axis: 'y',
      amount: 1
    },
    w: {
      axis: 'x',
      amount: -1
    },
    e: {
      axis: 'x',
      amount: 1
    }
  }
}
