// @flow

import Layer from '@/game/overworld/area/map/layer'

export default class TileLayer extends Layer {
  constructor (map: Game.Overworld.Area.Map, handleLoadPromise: Promise<*> => mixed, {
    width,
    height,
    name,
    type,
    visible,
    x,
    y,
    properties,
    opacity,
    draworder,
    data
  }: Object) {
    super(...arguments)
    const loadPromises = []
    _.each(data, (tileNum, index) => {
      if (tileNum > 0) {
        const ts = map.tilesets[0]
        const tile = ts.tileset.tiles[tileNum - ts.firstGID]
        loadPromises.push(tile.load())
        this.data.set(tile, (index % width), Math.floor(index / width))
      }
    })
    handleLoadPromise(Promise.all(loadPromises))
  }

  getSprite (x: number, y: number): ?PIXI.Sprite {
    const tile: Game.Overworld.Area.Map.Tileset.Tile = this.data.get(x, y)
    let sprite = null
    if (tile) {
      sprite = tile.getSprite()
      sprite.alpha = this.alpha
      sprite.visible = this.visible
    }
    return sprite
  }

  data: Game.util.Table<Game.Overworld.Area.Map.Tileset.Tile>
}
