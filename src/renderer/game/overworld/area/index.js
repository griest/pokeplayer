// @flow

import Game from '@/game'
import Map from './map'

export default class Area {
  static Map = Map

  constructor (handleLoadPromise: Function, {
    maps,
    width,
    height
  }: Object) {
    this.maps = maps
    this.width = width
    this.height = height
    this.data = new Game.util.Table()
    handleLoadPromise(Promise.all(_.map(maps, (xy, key) => game.getResource('map', key).then((map) => {
      for (let x = 0; x < map.width; x += 1) {
        for (let y = 0; y < map.height; y += 1) {
          const bottomLayer = map.getBottomLayerSpritesAt(x, y)
          const topLayer = map.getTopLayerSpritesAt(x, y)
          _.each([...bottomLayer, ...topLayer], sprite => {
            if (sprite) {
              sprite.position.set((xy.x + x) * game.constants.tileSize, (xy.y + y) * game.constants.tileSize)
            }
          })
          this.data.set({map: map, bottomLayer: bottomLayer, topLayer: topLayer}, xy.x + x, xy.y + y)
        }
      }
    }))))
  }

  relative (x: number, y: number): {|map: Map, x: number, y: number|} {
    const {map} = this.data.get(x, y)
    return {map: map, x: x - this.maps[map.id].x, y: y - this.maps[map.id].y}
  }

  absolute (map: string | Map, x: number, y: number): {|x: number, y: number|} {
    const mapID = typeof map === 'string' ? map : map.id
    return {x: x + this.maps[mapID].x, y: y + this.maps[mapID].y}
  }

  get ({map, x, y}: Object, relative: boolean = false): Object {
    let _x = x
    let _y = y
    if (relative) {
      const xy = this.absolute(map, x, y)
      _x = xy.x
      _y = xy.y
    }
    return this.data.get(_x, _y)
  }

  width: number
  height: number
  data: Game.util.Table<Object>
  maps: {[string]: {|x: number, y: number|}}
}
