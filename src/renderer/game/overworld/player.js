// @flow

import Actor from '@/game/overworld/area/map/object-layer/actor'

export const commandDirectionMap = {
  up: 'n',
  down: 's',
  left: 'w',
  right: 'e'
}

export function parseAnimations (anims: Object): {textures: Array<string>, animations: Array<Object>} {
  const textures = []
  const animations = _.map(anims, ({frames, next, speed}, name) => {
    return {name, next, speed, frames: frames.map(f => textures.includes(f) ? textures.indexOf(f) : textures.push(f) - 1)}
  })
  return {textures, animations}
}

const playerAnimations: {[string]: {frames: Array<string>, next: ?(string | number), speed: ?number}} = {};

// build player textures
['n', 's', 'w', 'e'].forEach(d => {
  // move
  ['walk', 'surf', 'dive'].forEach(m => {
    const name = `${d}${m}`
    playerAnimations[name] = {frames: [`${name}l`, `${name}`, `${name}r`, `${name}`], next: name, speed: 1}
    playerAnimations[`${name}f`] = {frames: [name], next: `${name}f`, speed: 1}
  });
  ['run', 'bike'].forEach(m => {
    const name = `${d}${m}`
    playerAnimations[name] = {frames: [`${name}l`, `${name}`, `${name}r`, `${name}`], next: name, speed: 2}
    playerAnimations[`${name}f`] = {frames: [name], next: `${name}f`, speed: 1}
  });
  // action
  ['fish', 'fishsurf'].forEach(a => {
    const name = `${d}${a}`
    playerAnimations[name] = {frames: [`${name}3`, `${name}2`, `${name}1`, `${name}0`], next: name, speed: 2}
  })
})

export default class Player extends Actor {
  constructor ({
    id,
    width,
    height,
    name,
    visible,
    type,
    x,
    y,
    rotation,
    gid,
    properties,
    images,
    animations,
    frames,
    framerate = 5
  }: Object = {}) {
    const anims = parseAnimations(playerAnimations)
    super(_.assign({}, {
      images: _.map(anims.textures, e => PIXI.Texture.from(`${game.paths.player}/boy/${e}.png`)),
      animations: anims.animations,
      frames: {
        width: 32,
        height: 48
      },
      width: 32,
      height: 48
    }, arguments[0]))
  }

  stopMove (): void {
    this.stop(`${this.direction}walkf`)
  }

  move (command: string, type: string): Promise<*> {
    const direction = commandDirectionMap[command]
    return this.animateMove({
      animation: `${direction}${type}`,
      shouldPlayAnimation: command !== game.scene.previousDirectionCommand,
      direction: direction,
      speed: game.constants.walkSpeed,
      beforeTween: payload => game.scene.playerTweened(payload)
    })
  }

  static animations = playerAnimations
}
