// @flow

export default class Sound {
  id: string
  src: string | {[string]: string}

  constructor ({
    id,
    src
  }: {|
    id: string,
    src: string | {[string]: string}
    |}) {
    createjs.Sound.registerSound(src, id)
  }

  play () {
    createjs.Sound.play(this.id)
  }
}
