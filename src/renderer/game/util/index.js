// @flow

import type {Stat, concatenatedProperty} from '@/game/types'
import Table from './table'

export {Table}

export function AbstractInstantiationError (obj: *): Error {
  return new TypeError('Do not instantiate `' + obj.constructor.name + '` directly, use a subclass instead')
}

export function displayObjects (...displayableModels: Array<{displayObject: PIXI.DisplayObject}>): Array<PIXI.DisplayObject> {
  return _.map(displayableModels, dm => dm.displayObject)
}

export function randomStats (max: number): Stat {
  return {
    hp: _.random(max),
    attack: _.random(max),
    defense: _.random(max),
    specialAttack: _.random(max),
    specialDefense: _.random(max),
    speed: _.random(max)
  }
}

export function calculateStats ({ class: pClass, evs, ivs, level }: Object): Stat {
  return {
    hp: 0,
    attack: 0,
    defense: 0,
    specialAttack: 0,
    specialDefense: 0,
    speed: 0
  }
}

export function createConcatenaryObject (options: Object = {}, properties: {[string]: concatenatedProperty}): Object {
  const obj = {}
  defineConcatenaryObject(obj, options, properties)
  return obj
}

export function defineConcatenaryObject (obj: Object, options: Object = {}, properties: {[string]: concatenatedProperty}): void {
  _.each(properties, (prop, k) => defineConcatenaryProperty(obj, options, k, prop))
}

export function defineConcatenaryProperty (obj: Object, options: Object, key: string, { value, prepend, append, options: propOptions }: concatenatedProperty): void {
  let pointer = obj[key]
  const separator = options.separator
  Object.defineProperty(obj, key, {
    enumerable: true,
    configurable: true,
    get: () => {
      let str = ''
      _.each(prepend, k => { str += obj[k] + separator })
      str += pointer
      _.each(append, k => { str += separator + obj[k] })
      return str
    },
    set: v => { pointer = v }
  })
  obj[key] = value
}

export function defineReactiveModel<T> (
  from: Object,
  to: Object,
  ...properties: Array<{
    from: string,
    to?: string,
    value?: T,
    compute?: T => T,
    extra?: T => void,
    override?: boolean
  }>): void {
  _.each(properties, opt => defineReactiveSetter({
    fromObj: from,
    toObj: to,
    fromProp: opt.from,
    toProp: opt.to,
    value: opt.value,
    compute: opt.compute,
    extra: opt.extra,
    override: opt.override
  }))
}

// heavily influenced by Vue's `defineReactive` function. Props to Evan!
export function defineReactiveSetter<T> ({
  fromObj,
  toObj,
  fromProp,
  toProp: toPropName,
  value,
  compute = v => v,
  extra = noop,
  override = false
}: {
    fromObj: Object,
    toObj: Object,
    fromProp: string,
    toProp?: string,
    value?: T,
    compute: T => T,
    extra: T => void,
    override?: boolean
  }): void {
  let setter: ?(T => void) = null

  const property = Object.getOwnPropertyDescriptor(fromObj, fromProp)
  if (property && property.configurable === false) {
    return
  }

  const toProp = toPropName || fromProp // if no prop on to object is specified, map to same property
  let val = fromObj[fromProp]

  // cater for pre-defined setters, unless the setter should override all others
  const oldSetter = !override && property && property.set

  // do all if checks while defining the setter, instead of inside it, should help performance a little
  if (oldSetter) {
    setter = function reactiveSetter (v) {
      oldSetter.call(fromObj, v)
      toObj[toProp] = compute.call(fromObj, v)
      extra.call(fromObj, v)
    }
  } else {
    setter = function reactiveSetter (v) {
      val = v
      toObj[toProp] = compute.call(fromObj, v)
      extra.call(fromObj, v)
    }
  }
  Object.defineProperty(fromObj, fromProp, {
    enumerable: true,
    configurable: true,
    get: () => val,
    set: setter
  })
  if (value) fromObj[fromProp] = value
}
