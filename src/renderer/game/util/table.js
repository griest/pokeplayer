// @flow

function init (data: *, index: number): * {
  if (!data[index]) data[index] = []
  return data
}

function getData (data: *, dims: Array<number>, initAll = false): * {
  if (dims.length === 1) {
    if (initAll) init(data, dims[0])
    return data[dims[0]]
  } else {
    init(data, dims[0])
    return getData(data[dims.shift()], dims, initAll)
  }
}

export default class Table<T> {
  constructor (dim: number = 2) {
    if (dim < 1 || dim !== Math.floor(dim)) throw new TypeError('Table dimension must be a positive integer')
    this.dim = dim
  }

  set (value: *, ...dims: Array<number>): void {
    const dimAry = this.dims(...dims)
    const index = dimAry.pop()
    getData(this.data, dimAry, true)[index] = value
  }

  get (...dims: Array<number>): * {
    return getData(this.data, this.dims(...dims))
  }

  dimensions (...dims: Array<number>): Array<number> {
    if (dims.length !== this.dim) console.log(`Warning, this table has dimension of ${this.dim}, and you passed in ${dims.length} dimensions.`)
    return dims.slice(0, this.dim)
  }

  dims (...dims: Array<number>): Array<number> {
    return this.dimensions(...dims)
  }

  each (cb: Function): void {
    const loop = (counter: number, indexes: number[], data: *): void => {
      if (counter < this.dim) {
        _.each(data, (e, i) => loop(counter + 1, [...indexes, i], e))
      } else if (data !== undefined) {
        cb(data, indexes)
      }
    }

    loop(0, [], this.data)
  }

  data: * = []
  dim: number = 2
}
