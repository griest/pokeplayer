// @flow

import * as _ from 'lodash'
import 'pixi.js/dist/pixi.js'
import 'pixi-actor'
import {Charm, Smoothie} from 'pexi'

import Effect from './effect'
import Item from './item'
import Pokemon from './pokemon'
import Scene from './scene'
import Overworld from './overworld'
import Sound from './sound'
import ResourceManager from './resource-manager'
import * as util from './util'

import Sandbox from '@/sandbox'

export default class Game extends PIXI.Application {
  static Effect = Effect
  static Item = Item
  static Pokemon = Pokemon
  static Overworld = Overworld
  static Scene = Scene
  static Sound = Sound
  static ResourceManager = ResourceManager
  static util = util

  constructor ({
    projectDir = '.',
    config,
    constants
  }: Object) {
    super({
      autoStart: false,
      // sharedTicker: true,
      width: Math.floor(window.innerWidth / 32) * 32,
      height: Math.floor(window.innerHeight / 32) * 32
    })
    window.game = this
    window.noop = (...args) => {}

    this.ticker.minFPS = 60
    this.ticker.add(this.tick, this)

    this.state = this.play

    this.loader.onError.add((...args) => console.log(...args))
    this.stage.interactiveChildren = false
    this.renderer.autoResize = true

    this.projectDir = projectDir
    this.paths.root = projectDir
    this.config = config
    this.constants = constants

    this.constants.walkSpeed = 15

    // this.fpsLabel = new PIXI.Text('- fps', {fontFamily: 'Arial', fontSize: 16, fill: 'white'})
    // this.fpsLabel.x = 10
    // this.fpsLabel.y = 20

    this.charm = new Charm(PIXI)
    this.smoothie = new Smoothie({
      engine: PIXI,
      renderer: this.renderer,
      root: this.stage,
      update: () => this.play.bind(this),
      properties: {tile: true}
    })

    // this.sandbox = new Overworld.Player({x: 2, y: 2}).displayObject
    // this.stage.addChild(this.sandbox)
    // this.start()

    this.init()
  }

  init (): void {
    window.onresize = this.resize.bind(this)
    this.loadProject().then(project => {
      this.name = project.name
      this.author = project.author
      this.getResource('area', 'tidewater').then(area => {
        let scenePromise
        this.scene = new Overworld(p => { scenePromise = p }, {area: area, map: 'va_beach', playerX: 20, playerY: 17})
        if (scenePromise) {
          scenePromise.then(() => {
            this.stage.addChild(this.scene.displayObject)
            this.cacheWorkaround = true
            this.loaded = true
            // this.render()
            // this.smoothie.start()
            // this.cache()
            this.start()
          }).catch(e => console.log(e))
        }
      }).catch(e => console.log(e))
    }).catch(e => console.log(e))
  }

  load (): Promise<*> {
    return new Promise((resolve, reject) => {
      fs.readdir('../../Pokemon-Virginia/data/map', (er, files) => {
        Promise.all(_.map(files, file => {
          // if (fs.statSync('../../Pokemon-Virginia/data/map/' + file).isFile()) return new Resource({type: 'map', path: file})
        })).then(resolve).catch(reject)
      })
    })
  }

  getResource (type: string, ...args: Array<*>): * {
    return this.resourceManager.get(type, ...args)
  }

  initResourceManager (project: Object): void {
    this.resourceManager = new ResourceManager()
    _.each({
      map: Overworld.Area.Map,
      tileset: Overworld.Area.Map.Tileset,
      pokemon: Pokemon,
      item: Item,
      area: Overworld.Area
    }, (val, key) => this.resourceManager.addResourceDef({type: key, dir: this.paths[key], ext: 'json', class: val}))
    _.each({
      player: 'png'
    }, (type, ext) => this.resourceManager.addResourceDef({type: type, dir: this.paths[type], ext: ext}))
  }

  loadProject (): Promise<*> {
    return new Promise((resolve, reject) => {
      this.loader.add('project', `${this.projectDir}/Game.json`, {crossOrigin: true}, resolve)
      this.loader.load()
    }).then(({data: project}) => {
      this.initResourceManager(project)
      return project
    })
  }

  resize (): void {
    const width = Math.floor(window.innerWidth / 32) * 32
    const height = Math.floor(window.innerHeight / 32) * 32
    if (this.renderer.width !== width || this.renderer.height !== height) {
      this.renderer.resize(width, height)
      this.scene.resize(width, height)
    }
  }

  cache (): void {
    this.scene.cache()
  }

  play (event: Object): void {
    this.scene.processInput(event)

    if (this.cacheWorkaround) {
      if (this.cacheWorkaroundCounter === 0) {
        this.cache()
        this.cacheWorkaround = false
      }
      this.cacheWorkaroundCounter--
    }
  }

  tick (event: Object): void {
    // this.fpsLabel.text = Math.round(this.ticker.FPS) + ' fps'

    this.state(event)

    this.charm.update()
    this.render()
  }

  name: string
  author: string | Array<string>
  config: Object = {}
  constants: Object = {}
  projectDir: string
  scene: Scene
  state: Function
  charm: Charm
  loaded: boolean = false
  currentMap: Overworld.Area.Map
  maps: Array<Overworld.Area.Map> = []
  fpsLabel: PIXI.Text
  cacheWorkaround: boolean = false
  cacheWorkaroundCounter: number = 3
  resourceManager: ResourceManager
  paths = util.createConcatenaryObject({separator: '/'}, {
    assets: {value: 'assets', prepend: ['root']},
    data: {value: 'data', prepend: ['root']},
    map: {value: 'map', prepend: ['data']},
    tileset: {value: 'tileset', prepend: ['data']},
    area: {value: 'area', prepend: ['data']},
    images: {value: 'images', prepend: ['assets']},
    objects: {value: 'objects', prepend: ['images']},
    actors: {value: 'actors', prepend: ['objects']},
    player: {value: 'player', prepend: ['actors']}
  })
}
