// @flow

import Ability from '@/game/pokemon/class/ability'
import EggGroup from '@/game/pokemon/class/egg-group'
import EvolutionList from '@/game/pokemon/class/evolution-list'
import Habitat from '@/game/pokemon/class/habitat'
import MoveSet from '@/game/pokemon/class/move-set'
import Type from '@/game/pokemon/class/type'

import type { Stat } from '@/game/types'

export default class PokemonClass {
  static Ability = Ability
  static EggGroup = EggGroup
  static EvolutionList = EvolutionList
  static Habitat = Habitat
  static MoveSet = MoveSet
  static Type = Type

  constructor ({
    number,
    stats,
    sprite,
    battleSprite,
    name,
    evolutions,
    levelMoves,
    tmMoves,
    hmMoves,
    eggMoves,
    description,
    cry,
    xpLevel,
    abilities,
    xpYield,
    evYield,
    catchRate,
    happiness,
    hatchSteps,
    eggGroups,
    height = 1,
    weight = 1,
    color = '',
    kind = '',
    hiddenAbilities = [],
    type1,
    type2
  }: Object) {
    this.number = game.pokedex.ensureUniqueNumber(number)
    this.stats = stats
    this.sprite = sprite
    this.battleSprite = battleSprite
    this.name = name
    this.evolutions = evolutions
    this.levelMoves = levelMoves
    this.tmMoves = tmMoves
    this.hmMoves = hmMoves
    this.eggMoves = eggMoves
    this.description = description || ''
    this.cry = cry
    this.xpLevel = xpLevel
    this.abilities = abilities
    this.xpYield = xpYield || game.constants.defaults.xpYield || 0
    this.evYield = evYield
    this.catchRate = catchRate || game.constants.defaults.catchRate || 5
    this.happiness = happiness || game.constants.defaults.happiness || 100
    this.hatchSteps = hatchSteps || game.constants.defaults.hatchSteps || 5000
    this.eggGroups = eggGroups
    this.height = height || 1
    this.weight = weight || 1
    this.color = color || ''
    this.kind = kind || ''
    this.hiddenAbilities = hiddenAbilities || []
    this.type1 = type1
    this.type2 = type2
  }

  getMoveList ({level}: {level: number}) {
    //
  }

  calculateStats ({evs, ivs, level}: Object) {

  }

  number: number // The pokedex number
  stats: Stat // The stat weights for the species, keys must match Game.stats
  sprite: PIXI.Sprite // The appearance of the pokemon ingame
  battleSprite: PIXI.Sprite // The appearance of the pokemon in battle
  name: string // The name of the species displayed ingame
  evolutions: EvolutionList // The possible evolutions of this species
  levelMoves: MoveSet // The moves the species learns by leveling up
  tmMoves: MoveSet // The moves the species learns by TM
  hmMoves: MoveSet // The moves the species learns by HM
  eggMoves: MoveSet
  description: string // A description of the species (displayed by pokedex)
  cry: Game.Sound // The sound that the species makes
  habitat: Array<Habitat> // The regions and maps this species can be found
  xpLevel: Array<number> // The total xp required to reach each level
  abilities: Array<Ability> // The abilities that this species can have
  hiddenAbilities: Array<Ability>
  xpYield: number
  evYield: Stat
  catchRate: number
  happiness: number
  hatchSteps: number
  eggGroups: EggGroup
  height: number
  weight: number
  color: string
  kind: string
  type1: Type
  type2: ?Type
}
