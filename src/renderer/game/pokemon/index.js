// @flow

import Game from '@/game'
import PClass from '@/game/pokemon/class'
import Nature from '@/game/pokemon/nature'
import Status from '@/game/pokemon/status'
import Move from '@/game/pokemon/move'

import type { Stat } from '@/game/types'

export default class Pokemon {
  static Class = PClass
  static Nature = Nature
  static Status = Status
  static Move = Move

  constructor ({
    name,
    level,
    xp,
    moves,
    class: pClass,
    ability,
    evs,
    ivs,
    nature,
    ot,
    secretID,
    owner,
    status,
    effects,
    happiness
  }: Object) {
    this.name = name.toString()
    this.level = level || game.constants.defaults.level
    this.class = pClass
    this.hp = this.class.stats.hp
    this.moves = moves || this.class.getMoveList({ level: this.level })
    this.xp = this.class.xpLevel[this.level]
    this.ability = ability || _.sample(this.class.abilities)
    this.evs = evs || Game.util.randomStats(game.constants.maxEV)
    this.ivs = ivs || Game.util.randomStats(game.constants.maxIV)
    this.stats = this.class.calculateStats({ evs: this.evs, ivs: this.ivs, level: this.level })
    this.nature = nature || _.sample(game.natures)
    this.ot = ot || owner
    this.secretID = _.random(game.constants.maxID)
    this.owner = owner
    this.status = status
    this.confused = false
    this.effects = effects || []
    this.happiness = happiness || this.class.happiness
  }

  name: string
  hp: number
  moves: Array<Move>
  class: PClass
  level: number
  xp: number
  ability: PClass.Ability
  evs: Stat
  ivs: Stat
  stats: Stat
  nature: Nature
  ot: string
  secretID: number
  owner: string
  status: Status
  confused: boolean
  effects: Array<Game.Effect>
  happiness: number
}
