// @flow

import { defineReactiveModel } from '@/game/util'

export default class DisplayableModel {
  constructor (displayObject: PIXI.DisplayObject, ...properties: Array<Object>) {
    this.displayObject = displayObject
    this.displayObject.snapToPixel = true
    this.defineReactiveProperties(...properties)
  }

  addChild (...children: Array<PIXI.DisplayObject>): void {
    const ary = _.compact(children)
    if (ary.length > 0) this.displayObject.addChild(...ary)
  }

  cache (): void {
    this.displayObject.cache(0, 0, this.displayObject.width, this.displayObject.height)
  }

  setX (x: number): void {
    this.x = x
    this.displayObject.x = x * game.constants.tileSize
  }

  setY (y: number): void {
    this.y = y
    this.displayObject.y = y * game.constants.tileSize
  }

  setPos (x: number, y: number): void {
    this.setX(x)
    this.setY(y)
  }

  getPosMod ({axis, amount}: Object): {|x: number, y: number|} {
    return this.constructor.getPosMod({x: this.x, y: this.y, axis, amount})
  }

  modPos ({axis, amount}: Object): {|x: number, y: number|} {
    const {x, y} = this.getPosMod(...arguments)
    this.x = x
    this.y = y
    return {x, y}
  }

  tweenMove ({axis, amount, speed, callback = noop}: {axis: string, amount: number, speed: number, callback?: typeof noop}): void {
    const {x, y} = this.constructor.getPosMod({x: this.displayObject.x, y: this.displayObject.y, axis, amount: amount * game.constants.tileSize})
    const tween = game.charm.slide(this.displayObject, x, y, speed, 'linear')
    tween.onComplete = callback
    tween.play()
  }

  defineReactiveProperties (...properties: Array<Object>): void {
    defineReactiveModel(this, this.displayObject, ...properties)
  }

  get x (): number {
    return this.pos.x
  }

  set x (value: number): void {
    this.pos.x = value
  }

  get y (): number {
    return this.pos.y
  }

  set y (value: number): void {
    this.pos.y = value
  }

  static getPosMod ({x, y, axis, amount}) {
    return {x: axis === 'x' ? x + amount : x, y: axis === 'y' ? y + amount : y}
  }

  displayObject: PIXI.DisplayObject
  pos: {|x: number, y: number|} = {x: 0, y: 0}
  height: number
  width: number
}
