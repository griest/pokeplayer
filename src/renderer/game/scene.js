// @flow

import DisplayableModel from '@/game/displayable-model'

export default class Scene extends DisplayableModel {
  constructor (displayObject: PIXI.DisplayObject, {
    height,
    width
  }: Object) {
    // if (new.target === Scene) throw new TypeError('`Scene` is an abstract class, subclass instead of instantiating.')

    super(displayObject, {
      from: 'width',
      value: width,
      compute: v => v * game.constants.tileSize
    }, {
      from: 'height',
      value: height,
      compute: v => v * game.constants.tileSize
    })

    document.addEventListener('keydown', this.keyDown.bind(this))
    document.addEventListener('keyup', this.keyUp.bind(this))
  }

  resize (width: number, height: number): void {
    // implement custom resize operation if you need to
  }

  isDirectionInput (key: string): boolean {
    return Object.keys(this.inputMap.direction).includes(key)
  }

  isButtonInput (key: string): boolean {
    return Object.keys(this.inputMap.button).includes(key)
  }

  keyDown ({key}: {key: string}): void {
    if (this.isDirectionInput(key) && !this.inputQueue.direction.includes(key)) this.inputQueue.direction.unshift(key)
    if (this.isButtonInput(key) && !this.inputQueue.button.includes(key)) this.inputQueue.button.unshift(key)
  }

  keyUp ({key}: {key: string}): void {
    let idx = this.inputQueue.direction.indexOf(key)
    if (idx > -1) this.inputQueue.direction.splice(idx, 1) // 25% faster than `_.remove`
    idx = this.inputQueue.button.indexOf(key)
    if (idx > -1) this.inputQueue.button.splice(idx, 1) // 25% faster than `_.remove`
  }

  processInput (event: Object): void {
    if (!this.blockInput) {
      if (this.inputQueue.direction.length === 0) {
        this.handleNoDirectionCommand()
      } else if (this.isDirectionInput(this.inputQueue.direction[0])) {
        this.handleDirectionCommand(this.inputMap.direction[this.inputQueue.direction[0]])
      }
      if (this.inputQueue.button.length > 0 && this.isButtonInput(this.inputQueue.button[0])) {
        this.handleButtonCommand(this.inputMap.button[this.inputQueue.button[0]])
      }
    }
  }

  handleDirectionCommand (command: string): void {
    throw Game.util.AbstractInstantiationError(this)
  }

  handleButtonCommand (command: string): void {
    throw Game.util.AbstractInstantiationError(this)
  }

  handleNoDirectionCommand (): void {
    throw Game.util.AbstractInstantiationError(this)
  }

  blockInput: boolean = false
  previousDirectionCommand: ?string
  inputQueue: {| direction: Array<string>, button: Array<string> |} = { direction: [], button: [] }
  inputMap: {| direction: Object, button: Object |} = {
    direction: {
      w: 'up',
      s: 'down',
      a: 'left',
      d: 'right'
    },
    button: {
      '\'': 'a',
      ';': 'b',
      '.': 'select',
      '/': 'start'
    }
  }
}
