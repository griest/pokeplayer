import typeof Game from '../src/model/Game'

declare var createjs: createjs
declare var game: Game
declare var loadJSON: loadJSON
declare var _: Lodash
declare var path: path
declare var axios: axios
declare var Benchmark: Benchmark
declare var fs: fs
declare var Q: Q
declare var noop: noop
declare var PIXI: PIXI
