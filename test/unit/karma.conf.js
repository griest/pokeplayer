'use strict'

const path = require('path')
const merge = require('webpack-merge')
const webpack = require('webpack')

const baseConfig = require('../../.electron-vue/webpack.renderer.config')
const projectRoot = path.resolve(__dirname, '../../src/renderer/game')

process.env.BABEL_ENV = 'test'

let webpackConfig = merge(baseConfig, {
  devtool: '#inline-source-map',
  plugins: [
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': '"testing"'
    }),
    new webpack.ProvidePlugin({
      Game: '@/game',
      sinon: 'sinon',
      chai: 'chai'
    })
  ]
})

// don't treat dependencies as externals
delete webpackConfig.entry
delete webpackConfig.externals
delete webpackConfig.output.libraryTarget

module.exports = config => {
  config.set({
    reporters: ['html', 'mocha', 'coverage'],
    coverageReporter: {
      dir: '../reports/coverage',
      reporters: [
        { type: 'lcov', subdir: '.' },
        { type: 'text-summary' }
      ]
    },
    htmlReporter: {
      outputFile: path.resolve(__dirname, '../reports/unit/index.html'),
      pageTitle: 'Pokeplayer Unit Test Report',
      groupSuites: true
    },
    browsers: [],
    customLaunchers: {
      'visibleElectron': {
        base: 'Electron',
        flags: ['--show']
      }
    },
    frameworks: ['mocha'],
    client: {
      useIframe: false,
      mocha: {
        fullTrace: true
      }
    },
    files: ['./index.js'],
    preprocessors: {
      './index.js': ['webpack', 'sourcemap']
    },
    webpack: webpackConfig,
    webpackMiddleware: {
      noInfo: true
    },
    singleRun: false,
    logLevel: config.LOG_WARN
  })
}
