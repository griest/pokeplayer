/* eslint-disable no-unused-expressions */
import Table from '@/game/util/table'

describe('Table', function () {
  beforeEach('init table', function () {
    this.t = new Table()
  })

  describe('.constructor', function () {
    it('should throw a TypeError for non-integer dimension', function () {
      (() => new Table(1.5)).should.throw(TypeError)
    })

    it('should throw a TypeError for negative dimension', function () {
      (() => new Table(-2)).should.throw(TypeError)
    })
  })

  describe('#set', function () {
    it('should set the value', function () {
      this.t.set(5, 1, 2)
      this.t.data[1][2].should.equal(5)
    })
  })

  describe('#get', function () {
    it('should get the value', function () {
      this.t.data[0] = [1, 5]
      this.t.data[3] = [null, 6]
      this.t.get(0, 1).should.equal(5)
      this.t.get(3, 1).should.equal(6)
    })
  })

  describe('#dims', function () {
    it('should truncate the dim list', function () {
      const dims = this.t.dims(2, 4, 6, 2)
      dims.should.have.lengthOf(2)
      dims[0].should.equal(2)
      dims[1].should.equal(4)
    })
  })

  describe('#each', function () {
    beforeEach('set some data on the table', function () {
      this.t.set('44', 4, 4)
      this.t.set('06', 0, 6)
    })

    it('should call the function once for each element', function () {
      const func = sinon.spy()

      this.t.each(func)

      func.should.have.been.calledTwice
    })

    it('should call the function with the correct arguments', function () {
      const func = (e, i) => {
        if (e === '44') {
          i.should.eql([4, 4])
        } else if (e === '06') {
          i.should.eql([0, 6])
        } else {
          throw new Error('the function was passed data not in the table')
        }
      }

      this.t.each(func)
    })
  })
})
