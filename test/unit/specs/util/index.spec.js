import * as util from '@/game/util'
import DisplayableModel from '@/game/displayable-model'
import 'pixi.js/dist/pixi.js'

window.noop = (...args) => {}

describe('util', function () {
  describe('#displayObjects', function () {
    const cont1 = new PIXI.Container()
    const cont2 = new PIXI.Container()
    util.displayObjects(new DisplayableModel(cont1), new DisplayableModel(cont2)).should.include.members([cont1, cont2])
  })

  describe('#randomStats', function () {
    let stats
    before(function () {
      stats = util.randomStats(2)
    })
    it('should not have values exceeding max = 2', function () {
      _.each(stats, (v, k) => v.should.not.be.above(2))
    })
    it('should have all the correct keys', function () {
      stats.should.be.an('object').that.has.all.keys('hp', 'attack', 'defense', 'specialAttack', 'specialDefense', 'speed')
    })
  })

  describe('#calculateStats', function () {
    it('should calculate the stats')
  })

  describe('#createConcatenaryObject', function () {
    it('should concatenate values', function () {
      const obj = util.createConcatenaryObject({separator: '/'}, {
        a: {value: 'a'},
        b: {value: 'b', prepend: 'a'},
        c: {value: 'c', append: 'b', prepend: 'a'}
      })
      obj.a.should.equal('a')
      obj.b.should.equal('a/b')
      obj.c.should.equal('a/c/a/b')
    })
  })

  describe('#defineReactiveModel', function () {
    it('should update values in target object', function () {
      const from = {str: 'a'}
      const to = {}
      const rm = util.defineReactiveModel(from, to, {
        from: 'num',
        to: 'square',
        compute: e => e * e,
        value: 5
      }, {
        from: 'str',
        to: 'replace',
        value: 'abc',
        compute: e => e.replace('a', 'z')
      }, {
        from: 'ary',
        value: [1, 2, 3],
        compute: e => _.map(e, i => i * i)
      })
      to.square.should.equal(25)
      to.replace.should.equal('zbc')
      to.ary.should.have.members([1, 4, 9])
      from.num = 3
      from.str = 'fa'
      from.ary = [3, 5]
      to.square.should.equal(9)
      to.replace.should.equal('fz')
      to.ary.should.have.members([9, 25])
    })
  })
})
