/* eslint-disable no-unused-expressions */
import ResourceManager from '@/game/resource-manager'

describe('ResourceManager', function () {
  describe('#addResourceDefs', function () {
    it('should call #addResourceDef', function () {
      const resources = [
        {
          type: 'array',
          class: Array,
          dir: '',
          ext: 'json'
        },
        {
          type: 'object',
          class: Object,
          dir: '',
          ext: 'json'
        }
      ]
      let rm = new ResourceManager()
      const spy = sinon.spy(rm, 'addResourceDef')
      spy.should.not.have.been.called
      rm.addResourceDefs(...resources)
      spy.should.have.been.calledTwice
      spy.should.have.been.calledWith(resources[0])
      spy.should.have.been.calledWith(resources[1])
    })
  })
})

/**
 * Stores the information about resource types and manages the efficient loading of those resources.
 */
// export default class ResourceManager {
//   static Loader = Loader

//   constructor (...resources: Array<{type: string, dir: string, ext: string, class: Function, argBuilder: (Promise<*> => void, Object) => Array<*>}>) {
//     this.addResourceDefs(...resources)
//   }

//   addResourceDefs (...resources: Array<{type: string, dir: string, ext: string, class: Function, argBuilder: (Promise<*> => void, Object) => Array<*>}>): void {
//     _.each(resources, res => this.addResourceDef(res))
//   }

//   addResourceDef ({type, dir, ext, class: klass, argBuilder}: Object): void {
//     this.Loaders[type] = loaderClassFactory(...arguments)
//   }

//   /**
//    * Gets a promise for some resource.
//    * Constructs a new loader for each resource since you can't add resources while loader is running.
//    * Enables on-demand loading of resources.
//    * 
//    * @param  {[type]}   type:       string        [description]
//    * @param  {[type]}   id:         string        [description]
//    * @param  {Function} argBuilder: (Promise<*>   [description]
//    * @return {[type]}               [description]
//    */
//   get (type: string, id: string, argBuilder?: Function): ?Promise<*> {
//     if (this.Loaders[type]) {
//       return new this.Loaders[type]().get(id, argBuilder)
//     } else {
//       console.warn('Resource type not found in manager, please add resource definition first')
//     }
//   }

//   /**
//    * Gets the basename and then strips the extension.
//    * @type {string}
//    */
//   static id (path: string): string {
//     return _.compact(path.split(/\\*\/|\.\w+$/)).reverse()[0]
//   }

//   Loaders: Object = {}
// }

// function loaderClassFactory ({type, dir, ext, class: klass, argBuilder}: Object): typeof Loader {
//   return class extends Loader {
//     constructor () {
//       super(dir, ext, klass, argBuilder)
//     }

//     /**
//      * Gets a promise for some resource.
//      * By storing data as promises immediately upon first request,
//      * we guarantee that each data will only have to be loaded and constructed once.    
//      * @param  {string}   id
//      * @param  {Function} argBuilder
//      * @return {Promise<*>}
//      */
//     get (id: string, argBuilder?: Function): Promise<klass> {
//       if (!this.constructor.data[id]) this.constructor.data[id] = super.get(...arguments)
//       return this.constructor.data[id]
//     }

//     static data: {[string]: klass} = {}
//   }
// }
