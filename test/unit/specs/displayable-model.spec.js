import DisplayableModel from '@/game/displayable-model'
import 'pixi.js/dist/pixi.js'

describe('DisplayableModel', function () {
  let cont, dm, child1, child2
  beforeEach('create display object and displayable model', function () {
    cont = new PIXI.Container()
    dm = new DisplayableModel(cont)
  })
  describe('#addChild', function () {
    child1 = new PIXI.Container()
    child2 = new PIXI.Container()
    it('should add the children to the display object', function () {
      dm.addChild(child1, child2)
      dm.displayObject.children.should.include.members([child1, child2])
    })
    it('should not add any children', function () {
      dm.displayObject.removeChildren()
      dm.addChild()
      dm.displayObject.children.should.have.lengthOf(0)
    })
    it('should only add 1 child', function () {
      dm.displayObject.removeChildren()
      dm.addChild(null, undefined, child1)
      dm.displayObject.children.should.include(child1)
    })
  })
})
